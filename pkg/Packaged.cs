// (PKG) Packaged.cs: all items packaged

package TranscendedMining {
  // OVERRIDE PLAYERTYPES =====================================================
  // Note: I intend to overwrite all classes of Players by doing this
  function armor::onTrigger(%self, %obj, %triggerNum, %val) {
    switch(%triggerNum) {
      case 0:
        %parent = parent::onTrigger(%self, %obj, %triggerNum, %val);
        %obj.TransM_HitScan(%val, %slot);
        return %parent;
      default:
        %parent = parent::onTrigger(%self, %obj, %triggerNum, %val);
        return %parent;
    }
  }
  // OVERRIDE MINIGAMES =======================================================
  function MiniGameSO::onAdd(%minigame) {
    // Return parent
    BlockdataSaver_Load("TransMOT"); // TranscendedMining Ores Table
    %parent = parent::onAdd(%minigame);
    %minigame.TransM_TicksElapsed = 0;
    %minigame.schedule(1000, "TransM_PlaceSpawns");
    %minigame.TransM_TimeTrigger();
    return %parent;
  }
  function MiniGameSO::addMember(%minigame, %client) {
    %parent = parent::addMember(%minigame, %client);
    %client.schedule(500, "applyPersistence");
    %client.schedule(1000, "TransM_DoStats");
    return %parent;
  }
};

deactivatePackage(TranscendedMining);
activatePackage(TranscendedMining);