// (SRC) Containers.cs: Drills and bombs
function MiniGameSO::TransM_EnqueueBomb(%minigame, %brick, %strength, %pos, %i) {
  if(!%brick) {
    %obj = containerFindFirst($TypeMasks::FxBrickObjectType, %pos, %i);
    if(isObject(%obj)) {
      %husk = new ScriptObject(HuskSO);
      %husk.members = 0;
      %husk.member0 = %obj;
      while(isObject(%next = containerSearchNext())) {
        %husk.member[%husk.members++] = %next;
      }
      return %minigame.TransM_KillHusk(%husk);
    } else { return %minigame; }
  } else {
    %pos = %brick.getPosition();
    
    return %minigame.TransM_EnqueueBomb(0, %strength, %pos, 0);
  }
}

function MiniGameSO::TransM_KillHusk(%minigame, %husk) {
  
}