// (SRC) Blocks.cs: and there be the mines

function MiniGameSO::TransM_KillBrick(%minigame, %x, %y, %z) {
  %brick = %minigame.brickHandler.brickAt[%x, %y, %z];
  if(!isObject(%brick)) {
    %brick = %minigame.TransM_PlaceMetaOre(%x, %y, %z);
  }
  %minigame.TransM_PlaceMetaOre(%brick.TransMX-1, %brick.TransMY, %brick.TransMZ);
  %minigame.TransM_PlaceMetaOre(%brick.TransMX+1, %brick.TransMY, %brick.TransMZ);
  %minigame.TransM_PlaceMetaOre(%brick.TransMX, %brick.TransMY-1, %brick.TransMZ);
  %minigame.TransM_PlaceMetaOre(%brick.TransMX, %brick.TransMY+1, %brick.TransMZ);
  %minigame.TransM_PlaceMetaOre(%brick.TransMX, %brick.TransMY, %brick.TransMZ-1);
  %minigame.TransM_PlaceMetaOre(%brick.TransMX, %brick.TransMY, %brick.TransMZ+1);  
  return %brick.schedule(100, "delete");
}

function MiniGameSO::TransM_PlaceICement(%minigame, %x, %y, %z) {
  if(BrickGroup_888888.brickHandler.brickAt[%x, %y, %z] !$= "") {
    return BrickGroup_888888.brickHandler.brickAt[%x, %y, %z];
  }
  %brick = new fxDTSBrick() {
    datablock = "brick4xCubeData";
    position = (%x * 2) SPC (%y * 2) SPC ((%z * 2) + 25000);
    rotation = "0 0 0 0";
    colorID = 47;
    scale = "1 1 1";
    angleID = "0";
    colorfxID = 0;
    shapefxID = 0;
    isPlanted = 1;
    stackBL_ID = 888888;
  };

  BrickGroup_888888.add(%brick);
  %brick.plant();
  %brick.setTrusted(1);
  %brick.onPlant();
  BrickGroup_888888.brickHandler.brickAt[%x, %y, %z] = %brick;
  return %brick;
}

function MiniGameSO::TransM_PlaceMetaOre(%minigame, %x, %y, %z) {
  if(BrickGroup_888888.brickHandler.brickAt[%x, %y, %z] !$= "") {
    return BrickGroup_888888.brickHandler.brickAt[%x, %y, %z];
  }
  %oreCoeff = getRandom();
  %orePick = getRandom($BDS::Tables::Count["TransMOT"]);
  %oreThing = $BDS::Tables::Entry["TransMOT", %orePick];
  %metaType = $TransM::Types::Block::Dirt;
  if(%oreCoeff < getField(%oreThing, 5)) {
    %depth = getField(%oreThing, 2);
    if(%depth $= "NULL") {
      %metaType = $TransM::Types::Block::Ore;
    } else {
      if(%depth < 0 && %depth > %z) {
        %metaType = $TransM::Types::Block::Ore;
      } else if(%depth > 0 && %depth < %z) {
        %metaType = $TransM::Types::Block::Ore;
      }
    }
  }
  
  %brick = new fxDTSBrick() {
    datablock = "brick4xCubeData";
    position = (%x * 2) SPC (%y * 2) SPC ((%z * 2) + 25000);
    TransMX = %x;
    TransMY = %y;
    TransMZ = %z;
    rotation = "0 0 0 0";
    colorID = 0;
    scale = "1 1 1";
    angleID = "0";
    colorfxID = 0;
    shapefxID = 0;
    isPlanted = 1;
    stackBL_ID = 888888;
    TransM_MetaType = %metaType;
    TransM_Damage = 0;
    TransM_PartyCount = 0;
  };
  
  switch(%metaType) {
    case $TransM::Types::Block::Dirt:
      %brick.TransM_Type = mFloatLength(%z / 2000, 0);
      %oreColor = getField($TransM::Dirt[%brick.TransM_Type], 1);
      %d = getField($TransM::Dirt[%brick.TransM_Type], 2);
    case $TransM::Types::Block::Ore:
      %brick.TransM_Type = %orePick;
      %oreColor = getField(%oreThing, 3);
      %d = getField(%oreThing, 1);
  }

  %brick.colorID = getWord(%oreColor, 0);
  %brick.colorFXID = getWord(%oreColor, 1);
  %brick.shapeFXID = getWord(%oreColor, 2);
  %d *= getRandom(90, 100);
  %d /= 50;
  %brick.TransM_MaxDamage = mFloatLength(%d, 0);
  BrickGroup_888888.add(%brick);
  %brick.plant();
  %brick.setTrusted(1);
  %brick.onPlant();
  
  BrickGroup_888888.brickHandler.brickAt[%x, %y, %z] = %brick;
  
  return %brick;
}

function MiniGameSO::TransM_PlaceSpawns(%minigame) {
  BrickGroup_888888.brickHandler = new ScriptObject(BrickHandlerSO);
  BrickGroup_888888.add(BrickGroup_888888.brickHandler);
  for(%x = -2; %x < 3; %x++) {
    for(%y = -2; %y < 3; %y++) {
      %brick = new fxDTSBrick() {
        datablock = "brickSpawnPointData";
        position = (%x * 2) SPC (%y * 2) SPC 25002.5;
        rotation = "0 0 0 0";
        colorID = 16;
        scale = "1 1 1";
        angleID = "0";
        colorfxID = 0;
        shapefxID = 0;
        isPlanted = 1;
        stackBL_ID = 888888;
      };
      
      BrickGroup_888888.add(%brick);
      %brick.plant();
      %brick.setTrusted(1);
      %brick.onPlant();
    }
  }
  %brick = new fxDTSBrick() {
    datablock = "brickMusicData";
    position = "0 0 25016";
    rotation = "0 0 0 0";
    colorID = 29;
    scale = "1 1 1";
    angleID = "0";
    colorfxID = 0;
    shapefxID = 0;
    isPlanted = 1;
    stackBL_ID = 888888;
  };
  
  BrickGroup_888888.add(%brick);
  %brick.plant();
  %brick.setTrusted(1);
  %brick.onPlant();
  
  %brick.setLight(BrightLight);
  %brick.setMusic(musicData_TranscendedMining_DASH_The_Caverns);

  for(%x = -6; %x < 7; %x++) {
    for(%y = -6; %y < 7; %y++) {
      %minigame.TransM_PlaceICement(%x, %y, 0);
      %minigame.TransM_PlaceICement(%x, %y, 9);
    }
  }
  for(%i=1; %i<9; %i++) {
    for(%j = -5; %j < 6; %j++) {
      %minigame.TransM_PlaceMetaOre(-6, %j, %i);
      %minigame.TransM_PlaceMetaOre(6, %j, %i);
      %minigame.TransM_PlaceMetaOre(%j, -6, %i);
      %minigame.TransM_PlaceMetaOre(%j, 6, %i);
    }
    %minigame.TransM_PlaceICement(-6, -6, %i);
    %minigame.TransM_PlaceICement(-6, 6, %i);
    %minigame.TransM_PlaceICement(6, -6, %i);
    %minigame.TransM_PlaceICement(6, 6, %i);
  }
  
  for(%x=-5; %x<6; %x++) {
    for(%y=-5; %y<6; %y++) {
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 1] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 2] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 3] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 4] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 5] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 6] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 7] = -1;
      BrickGroup_888888.brickHandler.brickAt[%x, %y, 8] = -1;
    }
  }
  
  return %minigame;
}