// (SRC) Money.cs: handle money
function GameConnection::TransM_AdjMoneyG(%client, %adj) {
  %client.TransM_MoneyG += %adj;
  return %client;
}

function GameConnection::TransM_AdjMoney(%client, %adj) {
  %client.TransM_MoneyM += %adj;
  if(%client.TransM_MoneyM > 1000) {
    %client.TransM_MoneyM -= mFloatLength(%adj / 1000, 0);
    %client.TransM_AdjMoneyG(%adj % 1000);
  }
  return %client;
}

function GameConnection::TransM_AdjMoneyK(%client, %adj) {
  %client.TransM_MoneyK += %adj;
  if(%client.TransM_MoneyK > 1000) {
    %client.TransM_MoneyK -= mFloatLength(%adj / 1000, 0);
    %client.TransM_AdjMoneyM(%adj % 1000);
  }
  return %client;
}

function GameConnection::TransM_AdjMoney(%client, %adj) {
  %client.TransM_MoneyB += %adj;
  if(%client.TransM_MoneyB > 1000) {
    %client.TransM_MoneyB -= mFloatLength(%adj / 1000, 0);
    %client.TransM_AdjMoneyK(%adj % 1000);
  }
  return %client;
}

function TransM_MoneyNotation(%g, %m, %k, %b) {
  %str = "$";
  if(%g != 0) { %str = %str @ %g @ "G"; }
  if(%m != 0) { %str = %str @ %m @ "M"; }
  if(%k != 0) { %str = %str @ %k @ "K"; }
  %str = %str @ %b;
    
  return %str;
}

function TransM_VerboseMoneyNotation(%client, %g, %m, %k, %b) {
  if(%g != 0) { %str = %str @ %g @ " billion(s) "; }
  if(%m != 0) { %str = %str @ %m @ " million(s) "; }
  if(%k != 0) { %str = %str @ %k @ " thousand(s) "; }
  %str = %str @ %b;
    
  return %str @ " dollars";
}