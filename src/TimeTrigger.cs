// (SRC) TimeTrigger.cs: A 10Hz Minigame Event Timer along with its dependents

// The trigger stimulus itself
function MiniGameSO::TransM_TimeTrigger(%minigame) {
  %minigame.TransM_OnTimeTrigger();
  %minigame.TransM_TicksElapsed++;
  %minigame.nextTimeTrigger = %minigame.schedule(100, "TransM_TimeTrigger");
  return %minigame;
}

// The other stuff which depends on the stimulus
function MiniGameSO::TransM_OnTimeTrigger(%minigame) {
  if(%minigame.TransM_TicksElapsed % 2400 == 0) {
    messageAll('', "BlockdataSaver is auto-saving ores...");
    BlockdataSaver_Save("TransMOT");
  }
  if(getBrickCount() > $Server::GhostLimit - 2500 || %minigame.TransM_IsClearing) {
    if(!%minigame.TransM_IsClearing) {
      %minigame.TransM_IsClearing = 1;
      messageAll('MsgAdminForce', "Clearing bricks in 5 seconds due to excess bricks.");
      %minigame.TransM_ClearSchedule = %minigame.schedule(5000, "TransM_Clear");
    } else if(%minigame.TransM_TicksElapsed % 50 == 0) {
      if(getBrickCount() == 0) {
        %minigame.TransM_IsClearing = 0;
        %minigame.schedule(500, "TransM_PlaceSpawns");
        %minigame.schedule(1000, "reset", 0);
      }
      %minigame.chatMessageAll('', "There are " @ getBrickCount() @ "/" @ $Server::GhostLimit @ " bricks left to be cleared.");
    }
    return %minigame.TransM_ClearSchedule;
  }
  for(%m = 0; %m < %minigame.numMembers; %m++) {
    %mm = %minigame.member[%m];
    %mm.setscore(%mm.TransM_Level);
    if(!isObject(%mm.player)) { continue; } // oops.
    if(%mm.player.TransM_Pos $= "" || %minigame.TransM_TicksElapsed % 10 == 0) {
      %mm.player.TransM_Pos = %mm.player.getPosition();
    }
    %mm.bottomPrint("<just:center>\c7Dirt: " @ %mm.TransM_Dirt @ " | \c5Pick: \c6" @ %mm.TransM_Level @ "\c7 | \c2Money: \c6" @ TransM_MoneyNotation(%mm.TransM_MoneyG, %mm.TransM_MoneyM, %mm.TransM_MoneyK, %mm.TransM_MoneyB) NL "\c5Bearing {\c4" @ VectorScale(VectorSub(%mm.player.TransM_Pos, "0 0 25001"), 0.5) @ "\c5}", 2, 1);
  }
  return %minigame;
}

function MiniGameSO::TransM_Clear(%minigame) {
  %minigame.TransM_IsClearing = 1;
  messageAll('MsgClearBricks', "A world clear has been queued.");
  %cnt = MainBrickGroup.getCount();
  for(%i = 0; %i < %cnt; %i++) { MainBrickGroup.getObject(%i).chainDeleteAll(); }
  return %minigame;
}