// (SRC) SaveHandling.cs: nice going piggybacking on player persistence

function GameConnection::TransM_DoStats(%client) {
  if(!%client.TransM) {
    %client.TransM = 1;
    %client.TransM_Level = 1;
    %client.TransM_MoneyG = 0;
    %client.TransM_MoneyM = 0;
    %client.TransM_MoneyK = 0;
    %client.TransM_MoneyB = 0;
    messageAll('', "\c1" @ %client.name @ " joined with clean stats.");
  }
  %client.TransM_Dirt = 0;
  %client.spawnPlayer();
}