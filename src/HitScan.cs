// (SRC) HitScan.cs: Hit stuff.

function Player::TransM_HitScan(%obj, %val, %slot) {
  if(%val && (%obj.currTool $= "" || %obj.currTool == -1) && !isEventPending(%obj.TransM_IsHitting) && (%obj.TransM_HitThrottle < getSimTime())) {
    %nph = %obj.gethackposition();
    %npe = vectorscale(%obj.getMuzzleVector(%slot), 5);
    %next = containerRaycast(%nph, VectorAdd(%nph, %npe), $TypeMasks::FxBrickObjectType);
    %obj.TransM_IsHitting = %obj.schedule(200 * (%obj.TransM_HitScanRateMod + 1), "TransM_HitScan", %val);
    %nextObj = getWord(%next, 0);
    if(isObject(%nextObj) && (%nextObj.TransM_MetaType != $TransM::Types::Block::PlayerBrick) && %nextObj.TransM_MaxDamage > 0) {
      %obj.playThread(3, activate);
      %nextObj.TransM_TryBreak(%obj, $TransM::Types::Hitscan::Direct, 1.0);
    }
  } else {
    if(isEventPending(%obj.TransM_IsHitting)) {
      if(!%val) { %obj.TransM_HitThrottle = getSimTime() + (200 * (%obj.TransM_HitScanRateMod + 1)); }
      cancel(%obj.TransM_IsHitting);
      %obj.TransM_IsHitting = -1;
    }
  }
  return %obj.TransM_IsHitting;
}

function fxDTSBrick::TransM_GetName(%brick) {
  switch(%brick.TransM_MetaType) {
    case $TransM::Types::Block::Dirt:
      return getField($TransM::Dirt[%brick.TransM_Type], 0);
    case $TransM::Types::Block::Ore:
      return getField($BDS::Tables::Entry["TransMOT", %brick.TransM_Type], 0);
    default: return "ERROR";
  }
}

function fxDTSBrick::TransM_TryBreak(%brick, %breaker, %type, %damage) {
  if($DefaultMiniGame.TransM_IsClearing) { return %brick; }
  %bcl = %breaker.client;
  if(isObject(%bcl)) {
    if(%brick.TransM_PartyCount > 0) {
      for(%p = 0; %p < %brick.TransM_PartyCount; %p++) {    
        if(%brick.TransM_PartyBLID[%p] == %bcl.getBLID()) {
          %thisp = %p;
          break;
        }
      }
    }
    if(%thisp $= "") {
      %thisp = %brick.TransM_PartyCount;
      %brick.TransM_PartyCount++;
      %brick.TransM_PartyBLID[%thisp] = %bcl.getBLID();
    }
    %msgBcl = 1;
  } else {
    if(%brick.TransM_PartyCount > 0) {
      for(%p = 0; %p < %brick.TransM_PartyCount; %p++) {    
        if(%brick.TransM_PartyBLID[%p] == 888888) {
          %thisp = %p;
          break;
        }
      }
    }
    if(%thisp $= "") {
      %thisp = %brick.TransM_PartyCount;
      %brick.TransM_PartyCount++;
      %brick.TransM_PartyBLID[%thisp] = 888888;
    }
  }
  %brick.playSound(hammerHitSound);
  %breakD = mFloatLength(%bcl.TransM_Level * %damage, 0);
  %newBreak = %breakD + %brick.TransM_Damage;
  if(%newBreak > %brick.TransM_MaxDamage) {
    if(%msgBcl) { %bcl.centerPrint("Mined " @ %brick.TransM_GetName(), 2); }
    
    if(%brick.TransM_MetaType == $TransM::Types::Block::Ore) {
      %bcl.TransM_AdjMoney(getField($BDS::Tables::Entry["TransMOT", %brick.TransM_Type], 1));
    } else if(%brick.TransM_MetaType == $TransM::Types::Block::Dirt) {
      %bcl.TransM_Dirt++;
    }
    
    $DefaultMiniGame.TransM_KillBrick(%brick.TransMX, %brick.TransMY, %brick.TransMZ);
  } else {
    if(%msgBcl) {
      %bcl.centerPrint("Mining " @ %brick.TransM_GetName() NL "Hardness: " @ %brick.TransM_MaxDamage - %brick.TransM_Damage, 2);
      %brick.TransM_Damage = %newBreak;
    }
  }
  return %brick;
}