// (SRC) Commands.cs: I need moreeeee

function GameConnection::TransM_IsFlooding(%client) {
  %flood = %client.TransM_CmdFlood > (getSimTime() + 1000);
  if(!%flood) { %client.TransM_CmdFlood = getSimTime(); }
  return %flood;
}

function serverCmdUpgradePick(%client) {
  if(%client.TransM_IsFlooding) { return %client; }
  %pick = %client.TransM_Level + 1;
  %amt = %pick * 25;
  if(%amt > ((%client.TransM_MoneyK * 1000) + %client.TransM_MoneyB)) {
    messageClient(%client, '', "A level " @ %pick @ " pick costs " @ %amt @ " which you can not afford.");
    return %client;
  }
  %client.playSound(Beep_Checkout_Sound);
  %client.TransM_AdjMoney(-%amt);
  messageClient(%client, '', "You now have a level " @ %pick @ " pick.");
  %client.TransM_Level++;
}

function serverCmdDebug(%client, %arg0, %arg1, %arg2) {
  if(!%client.isAdmin) { return %client; }
  warn(%client.name @ " is using the debugger - {" @ %arg0 @ "," @ %arg1 @ "," @ %arg2 @ "}");
  switch$(%arg0) {
    case "resetBL_ID":
      if(%arg1 $= %client.TransM_ResetHash && %client.TransM_ResetHash !$= "") {
        %arg1c = findClientByBL_ID(%arg1);
        messageClient(%client, '', "Reset playerBL_ID " @ %arg1);
        %arg1c.TransM = "";
        %arg1c.TransM_DoStats();
        return %client;
      }
      
      %client.TransM_ResetHash = getSubStr(sha1(getRandom()), 0, 6);
      messageClient(%client, '', "Please enter this hash to confirm player reset: " @ %client.TransM_ResetHash);
    case "clear":
      messageClient(%client, '', "Clearing by debugger...");
      $DefaultMiniGame.TransM_Clear();
    case "bomb":
      messageClient(%client, '', "Placing bomb by debugger...");
      $DefaultMiniGame.TransM_EnqueueBomb(%arg1);
  }
  return %client;
}