// server.cs: strap everything.
$TransM::Types::Block::PlayerBrick = 0;
$TransM::Types::Block::Dirt = 1;
$TransM::Types::Block::Ore = 2;
$TransM::Types::Block::Special = 3;

$TransM::Types::Hitscan::Direct = 1;
$TransM::Types::Hitscan::Indirect = 2;

BlockdataSaver_Load("TransMOT");
if(!$BDS::Tables::Count["TransMOT"]) {
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Einsteinium" TAB 5 TAB -50 TAB "21 0 0" TAB 8 TAB 0.02 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Black Metal" TAB 50 TAB -100 TAB "63 0 0" TAB 8 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Altiar 4" TAB 120 TAB -200 TAB "1 0 0" TAB 8 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Graphite" TAB 225 TAB -300 TAB "60 0 0" TAB 8 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Titanium Bonds" TAB 300 TAB -400 TAB "13 0 0" TAB 7 TAB 0.007 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Molecular Bonds" TAB 500 TAB -500 TAB "15 0 0" TAB 7 TAB 0.006 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Tachyon Weavings" TAB 700 TAB -600 TAB "20 0 0" TAB 7 TAB 0.005 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Niobium Alloy" TAB 1000 TAB -700 TAB "19 0 0" TAB 6 TAB 0.004 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Mercury Nitrates" TAB 3000 TAB -850 TAB "16 0 0" TAB 5 TAB 0.003 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Tachyon Nitrates" TAB 5000 TAB -1050 TAB "19 0 0" TAB 5 TAB 0.002 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Diamond II" TAB 7000 TAB -1500 TAB "27 0 0" TAB 5 TAB 0.002 TAB 888888;

  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Gold" TAB 30 TAB "NULL" TAB "23 0 0" TAB 10 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Silver" TAB 15 TAB "NULL" TAB "59 0 0" TAB 11 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Diamond I" TAB 100 TAB "NULL" TAB "29 0 0" TAB 5 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Platinum" TAB 50 TAB "NULL" TAB "46 0 0" TAB 10 TAB 0.01 TAB 888888;
  $BDS::Tables::Entry["TransMOT", $BDS::Tables::Count["TransMOT"]++] = "Epicness" TAB 1000000 TAB -6000 TAB "6 3 0" TAB 2 TAB 0.0001 TAB 888888;
}
BlockdataSaver_Save("TransMOT");
$TransM::Dirt[1] = "multiverse fragments" TAB "0 3 1" TAB "1000";
$TransM::Dirt[0] = "upper dirt" TAB "45 0 0" TAB "5";
$TransM::Dirt[-0] = $TransM::Dirt[0];
$TransM::Dirt[-1] = "middle dirt" TAB "43 0 0" TAB "10";
$TransM::Dirt[-2] = $TransM::Dirt[-1];
$TransM::Dirt[-3] = "lower dirt" TAB "58 0 0" TAB "25";
$TransM::Dirt[-4] = "bedrock" TAB "62 1 0" TAB "50";
$TransM::Dirt[-5] = "upper mantle" TAB "33 0 0" TAB "100";
$TransM::Dirt[-6] = "middle mantle" TAB "0 0 0" TAB "250";
$TransM::Dirt[-7] = $TransM::Dirt[-6];
$TransM::Dirt[-8] = "lower mantle" TAB "1 3 0" TAB "500";
$TransM::Dirt[-9] = "outer core" TAB "2 3 2" TAB "1000";
$TransM::Dirt[-10] = "middle core" TAB "10 3 2" TAB "2500";
$TransM::Dirt[-11] = "inner core" TAB "56 3 2" TAB "5000";


// Do we have a file?
RegisterPersistenceVar("TransM", false, "");
// What is our pick level?
RegisterPersistenceVar("TransM_Level", false, "");
// How much money do we have?
RegisterPersistenceVar("TransM_MoneyB", false, ""); // 1 money
RegisterPersistenceVar("TransM_MoneyK", false, ""); // 1K money
RegisterPersistenceVar("TransM_MoneyM", false, ""); // 1M money
RegisterPersistenceVar("TransM_MoneyG", false, ""); // 1G money

exec("./lib/BlockdataSaver.cs");
exec("./lib/Triangular.cs");
exec("./pkg/Packaged.cs");
exec("./src/Blocks.cs");
exec("./src/Commands.cs");
exec("./src/Containers.cs");
exec("./src/HitScan.cs");
exec("./src/Money.cs");
exec("./src/SaveHandling.cs");
exec("./src/TimeTrigger.cs");