// (LIB) BlockdataSaver.cs: save values by tabbed fields
function BlockdataSaver_Load(%table) {
  warn("loading BDS table" SPC %table);
  %fo = new FileObject();
  %fo.openForRead("config/server/BDS/" @ %table @ ".txt");
  $BDS::Tables::Count[%table] = 0;
  while(!%fo.isEOF()) {
    $BDS::Tables::Entry[%table, $BDS::Tables::Count[%table]++] = %fo.readLine();
  }
  %fo.close();
  %fo.delete();
  warn("loaded BDS table" SPC %table);
}

function BlockdataSaver_Save(%table) {
  warn("saving BDS table" SPC %table);
  %fo = new FileObject();
  %fo.openForWrite("config/server/BDS/" @ %table @ ".txt");
  for(%i = 1; %i <= $BDS::Tables::Count[%table]; %i++) {
    %fo.writeLine($BDS::Tables::Entry[%table, %i]);
  }
  %fo.close();
  %fo.delete();
  warn("saved BDS table" SPC %table);
}
