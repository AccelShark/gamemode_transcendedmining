// (LIB) Bicubic.cs: bicubic interpolation
function interpolateCubic(%arr, %t) {
  for(%i = 0; %i < 4; %i++) {
    %x[%i] = getWord(%arr, %i);
  }
  %co[0] = ((2 * mPow(%t, 3)) + (-3 * mPow(%t, 2)) + 1) * %x[1];
  %co[1] = ((mPow(%t, 3)) + (-2 * mPow(%t, 2)) + %t) * %x[0];
  %co[2] = (-2 * (mPow(%t, 3)) + (3 * mPow(%t, 2))) * %x[2];
  %co[3] = ((mPow(%t, 3)) + (-1 * mPow(%t, 2))) * %x[3];
  return %co[0] + %co[1] + %co[2] + %co[3];
}

function interpolateBicubic(%arr, %mag) {
  for(%i = 0; %i < 16; %i++) {
    %co[%i] = getWord(%arr, %i);
  }
  for(%y = 0; %y < 4; %y++) {
    for(%x = 0; %x < %mag; %x++) {
      %xx = %x / %mag;
      %zco0[%xx, %y] = interpolateCubic(%co[(%y * 4) + 0] SPC %co[(%y * 4) + 1] SPC %co[(%y * 4) + 2] SPC %co[(%y * 4) + 3], %xx);
    }
  }
  for(%y = 0; %y < %mag; %y++) {
    for(%x = 0; %x < %mag; %x++) {
      %xx = %x / %mag;
      %yy = %y / %mag;
      // it is critically important to set these diff...
      %zco1[%xx, %yy] = interpolateCubic(%zco0[%xx, 0] SPC %zco0[%xx, 1] SPC %zco0[%xx, 2] SPC %zco0[%xx, 3], %yy);
      
      %zco2 = %zco2 SPC %zco1[%xx, %yy];
    }
  }
  return ltrim(%zco2);
}
